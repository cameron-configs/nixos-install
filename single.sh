#!/usr/bin/env bash
set -xuf
trap read debug

USER=cameron

DISK="/dev/$1"
P="parted $DISK"

# Start
$P -- mklabel gpt

# GRUB partitions
GRUB1="${DISK}1"
$P -- mkpart primary 1MiB 100MiB
GRUB2="${DISK}2"
$P -- mkpart ESP fat32 101MiB 600MiB

# NixOS partition
NIX="${DISK}3"
$P -- mkpart primary 601MiB 953000MiB

# GRUB partitions flags
$P -- set 1 bios_grub on
$P -- set 2 boot on

# Encrypt NixOS partition
cryptsetup luksFormat $NIX
cryptsetup luksOpen $NIX enc-pv
pvcreate /dev/mapper/enc-pv
vgcreate vg /dev/mapper/enc-pv
lvcreate -n swap vg -L 32G
lvcreate -n root vg -l 100%FREE

# Format GRUB partition
mkfs.vfat -n BOOT $GRUB2

# Format NixOS partition
mkfs.ext4 -L root /dev/vg/root
mkswap -L swap /dev/vg/swap

# Mount NixOS partitions
mount /dev/vg/root /mnt
mkdir /mnt/boot
mount $GRUB2 /mnt/boot
swapon /dev/vg/swap

# Install NixOS 
mkdir -p /mnt/etc/nixos
nixos-generate-config --root /mnt
nixos-install --root /mnt

