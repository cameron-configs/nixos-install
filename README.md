# Setup Dualboot System
Example here assumes disk for install is /dev/sda, but you should the name of your disk with `lsblk` and use that instead.
This should be able to run from any debian based system, though it's only been tested from an Ubuntu 20.04 live usb.

    ./dualboot_and_file_part.sh sda
