# Install nix package manager
sudo apt update
sudo apt install curl
sh <(curl -L https://nixos.org/nix/install) --no-daemon
source $HOME/.nix-profile/etc/profile.d/nix.sh

# Use NixOS 21.05 channel
nix-channel --add https://nixos.org/channels/nixos-21.05 nixpkgs
nix-channel --update
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixpkgs"

# Install packages
nix-env -i git vim nixos-install-tools

# Pull in nixos config submodule
git submodule update --init

